#-*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import itertools
import pyhsmm
from pyhsmm.util.text import progprint_xrange
import pandas as pd
from scipy import stats
from sklearn.preprocessing import normalize
from sklearn.decomposition import PCA
np.seterr(divide='ignore')  # these warnings are usually harmless for this code


def read_econmic_data(path):
    economic_df = pd.read_csv(path, encoding = 'euc-kr')
    economic_df_rm = economic_df.drop(economic_df.columns[69], 1).drop(economic_df.columns[76], 1).drop('month', 1).drop('year', 1)
    economic_matrix = economic_df_rm.as_matrix()
    micro_economic_data = economic_matrix[:,0:62]
    macro_economic_data = economic_matrix[:,62:84]
    dti = economic_matrix[:,84]
    ltv = economic_matrix[:,85]
    micro_economic_data_zscore = stats.zscore(micro_economic_data, axis=0)
    macro_economic_data_zscore = stats.zscore(macro_economic_data, axis=0)
    micro_economic_data_norm = normalize(micro_economic_data, axis=0)
    macro_economic_data_norm = normalize(macro_economic_data, axis=0)
    full_economic_data_zscore = np.concatenate((micro_economic_data_zscore, macro_economic_data_zscore), axis=1)
    pca_2 = PCA(n_components=2)
    pca_4 = PCA(n_components=4)
    pca_8 = PCA(n_components=8)
    micro_economic_data_zscore_pca_2 = pca_2.fit(micro_economic_data_zscore).transform(micro_economic_data_zscore)
    micro_economic_data_zscore_pca_4 = pca_4.fit(micro_economic_data_zscore).transform(micro_economic_data_zscore)
    micro_economic_data_zscore_pca_8 = pca_8.fit(micro_economic_data_zscore).transform(micro_economic_data_zscore)
    macro_economic_data_zscore_pca_2 = pca_2.fit(macro_economic_data_zscore).transform(macro_economic_data_zscore)
    macro_economic_data_zscore_pca_4 = pca_4.fit(macro_economic_data_zscore).transform(macro_economic_data_zscore)
    macro_economic_data_zscore_pca_8 = pca_8.fit(macro_economic_data_zscore).transform(macro_economic_data_zscore)
    full_economic_data_zscore_pca_2 = pca_2.fit(full_economic_data_zscore).transform(full_economic_data_zscore)
    full_economic_data_zscore_pca_4 = pca_4.fit(full_economic_data_zscore).transform(full_economic_data_zscore)
    full_economic_data_zscore_pca_8 = pca_8.fit(full_economic_data_zscore).transform(full_economic_data_zscore)
    datas = dict()
    datas["micro_economic_data"] = micro_economic_data
    datas["macro_economic_data"] = macro_economic_data
    datas["micro_economic_data_zscore"] = micro_economic_data_zscore
    datas["macro_economic_data_zscore"] = macro_economic_data_zscore
    datas["micro_economic_data_norm"] = micro_economic_data_norm
    datas["macro_economic_data_norm"] = macro_economic_data_norm
    datas["micro_economic_data_zscore_pca_2"] = micro_economic_data_zscore_pca_2
    datas["micro_economic_data_zscore_pca_4"] = micro_economic_data_zscore_pca_4
    datas["micro_economic_data_zscore_pca_8"] = micro_economic_data_zscore_pca_8
    datas["macro_economic_data_zscore_pca_2"] = macro_economic_data_zscore_pca_2
    datas["macro_economic_data_zscore_pca_4"] = macro_economic_data_zscore_pca_4
    datas["macro_economic_data_zscore_pca_8"] = macro_economic_data_zscore_pca_8
    datas["full_economic_data_zscore_pca_2"] = full_economic_data_zscore_pca_2
    datas["full_economic_data_zscore_pca_4"] = full_economic_data_zscore_pca_4
    datas["full_economic_data_zscore_pca_8"] = full_economic_data_zscore_pca_8
    datas["full_economic_data_zscore"] = full_economic_data_zscore
    groundtruth_states = []
    gts = dict()
    num = 0
    for state in dti:
        if state in gts:
            groundtruth_states.append(gts[state])
        else:
            gts[state] = num
            groundtruth_states.append(gts[state])
            num += 1
    return datas, groundtruth_states, dti, ltv

#data_name = "macro_economic_data_zscore_pca_2"
#print(data_name)
datas, groundtruth_states, dti, ltv = read_econmic_data("../data/data.csv")
f = open("economic_results.csv", "a")

for data_name in datas:
    data = datas[data_name]
    # initialize hdphsmm static parameter
    hdphsmm = pyhsmm.models.MultiParamWeakLimitHDPHSMM(obs_num=datas[data_name].shape[1])

    # make parameter for hdp
    alpha_gamma_alpha_0_list = itertools.product([1., 5., 10.], [1., 5., 10.], [2*3,2*6,2*12])

    # add models
    for alpha, gamma, alpha_0 in alpha_gamma_alpha_0_list:
        hdphsmm.add_model(alpha, gamma, alpha_0, 2)

    # set data for hdphsmm
    hdphsmm.set_data(datas[data_name])

    # hdphsmm start inference with 50th iterations
    stateseqs = hdphsmm.inference(3)
    # save stateseqs

    # evaluate between groundtruth and models
    best_model_index, best_model_accuracy, accuracys = hdphsmm.evaluate(groundtruth_states)
    print(str(best_model_index)+"th model : " + str(best_model_accuracy))
    result = data_name +", " + str(best_model_index)+", " + str(best_model_accuracy) +"\n"
    f.write(result)

    # save plot
    hdphsmm.plot(save_dir=data_name, prefix="my_ex")
    hdphsmm = ""

f.close()
