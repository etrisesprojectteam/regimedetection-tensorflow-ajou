
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import itertools
import pyhsmm
from pyhsmm.util.text import progprint_xrange
import pandas as pd
np.seterr(divide='ignore')  # these warnings are usually harmless for this code

print('''
This demo shows the HDP-HSMM in action. Its iterations are slower than those
for the (Sticky-)HDP-HMM, but explicit duration modeling can be a big advantage
for conditioning the prior or for discovering structure in data.
''')


###################
#  generate data  #
##################

def generate_data(sample_num, iterate_sampling, obs_num):
    # np.random.seed(1)
    np.seterr(invalid='raise')

    kappa_0_num = 0.01 * (obs_num + 2)
    nu_0_num = obs_num + 2
    num_modes = 3
    groundtruth_states = []

    for i in range(iterate_sampling):
        for j in range(sample_num):
            groundtruth_states.append(i % num_modes)

    obs_hypparams = dict(mu_0=np.zeros(obs_num),
                         sigma_0=np.eye(obs_num),
                         kappa_0=kappa_0_num,
                         nu_0=nu_0_num)

    true_obs_distns = [pyhsmm.distributions.Gaussian(**obs_hypparams) for i in range(num_modes)]
    data = np.concatenate([true_obs_distns[i % num_modes].rvs(sample_num) for i in range(iterate_sampling)])

    return data, obs_hypparams,groundtruth_states

###############
#  load data  #
###############

data, obs_hypparams, groundtruth = generate_data(sample_num=3,
                                    iterate_sampling=40,
                                    obs_num=10)

#########################
#  posterior inference  #
#########################

# initialize hdphsmm static parameter
hdphsmm = pyhsmm.models.MultiParamWeakLimitHDPHSMM(obs_num=data.shape[1])

# make parameter for hdp
alpha_gamma_list = itertools.product([1e1] * 10,
                                     [1e1] * 10)

# add models
for alpha, gamma in alpha_gamma_list:
    hdphsmm.add_model(alpha, gamma, 6, 2)

# set data for hdphsmm
hdphsmm.set_data(data)

# hdphsmm start inference with 50th iterations
stateseqs = hdphsmm.inference(3)
# save stateseqs

# evaluate between groundtruth and models
best_model_index, best_model_accuracy, accuracys = hdphsmm.evaluate(groundtruth)
print(str(best_model_index)+"th model : " + str(best_model_accuracy))

# save plot
hdphsmm.plot(save_dir="fig", prefix="my_ex")

# hdphsmm.clear_model()
