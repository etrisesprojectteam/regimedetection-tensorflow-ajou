# HDP-HSMM을 위한 대규모 통계적 파라미터 분산 추론 소프트웨어#
![system_structure_overview](image/system_structure_overview.png)

## Requirements and Install Guide
NVIDIA GPU의 연산 유닛인 CUDA를 이용한 병렬화를 위해 Tensorflow를 설치해야 한다. Tensorflow는 [tensorflow setup](https://www.tensorflow.org/versions/r0.11/get_started/os_setup.html#download-and-setup) 에서 다운 및 설치 방법을 확인 할 수 있으며, 현재 본 라이브러리는 tensorflow 0.11.0 버전을 기준으로 테스트 되어있다. Tensorflow 설치 시 권장하는 GPU 환경은 **CUDA Toolkit 8.0, cuDNN v5** 이다.  
Tensorflow 이외에 필요 라이브러리를 아래와 같이 설치한다.

```shell
$ pip install -r requirements.txt
```

그 후 `install.sh` 스크립트를 실행하여 pyhsmm을 빌드 및 설치한다.

```shell
$ sh install.sh
```

## API Description ##
### Class pyhsmm.models.MultiParamWeakLimitHDPHSMM()
HDP-HSMM에 Multiple Parameter를 동시에 수행하는 클래스로서, Tensorflow 기반의 GibbsSampling 알고리즘을 수행한다.   

- - -

###### pyhsmm.models.MultiParamWeakLimitHDPHSMM.\_\_init__(obs_num, trunc=None, Nmax=25)
생성자로서, 고정 파라미터에 따른 MultiParamWeakLimitHDPHSMM 모델을 생성한다.

###### Args:
* obs_num : Observation 분포인 Multivariate Gaussian 또는 Multinomial에서의 다차원 변수의 차원 수로서, 데이터의 Feature 개수와 같다.Numpy 객체인 경우 data.shape[1] 을 사용하면 된다.

* trunc : Truncation에 대한 파라미터로서, ?? default 값은 None 이다.

* Nmax : HDP에서 Hidden state 개수를 판단할 때, 무한의 개수로서 찾는 것이 아니라 Nmax 이하에서 Hidden state 개수를 추론하게 된다. default 값은 25이다. (ex. Nmax = 25 -> 25개 이하에서 Hidden state 개수를 추론)

- - -

###### pyhsmm.models.MultiParamWeakLimitHDPHSMM.set_data(data)
데이터를 설정해준다. 만약 기존의 데이터가 설정되어 있다면, 기존 데이터는 초기화하고 새로운 데이터로 설정해준다.

###### Args:
* data : list of list 또는 numpy 배열 [Data size][Observation Num] 인 데이터를 넣어 주어야 한다.

- - -

###### pyhsmm.models.MultiParamWeakLimitHDPHSMM.add_model(alpha, gamma, alpha_0, beta_0)
들어오는 파라미터에 대한 모델을 추가해준다. 만약 여러개의 파라미터를 동시에 수행하기 위해서는 add_model()을 여러번 불러주어야 한다.

만약에 Duration 분포의 Duration 길이를 n 으로 관찰 하고 싶다면, $$ \frac{alpha_0}{beta_0} = n $$ 으로 설정 되어야 한다. 예를 들어, alpha_0 = 2*3, beta_0 = 2 이면, Duration의 길이는 2 정도를 관찰 하게 된다.

###### Args:
* alpha : HDP를 위한 Second concentration Level 하이퍼 파라미터로서, alpha > 0 이어야 한다
* gamma : HDP를 위한 First concentration Level 하이퍼 파라미터로서, gamma > 0 이어야 한다.
* alpha_0 : Duration 분포의 하이퍼 파라미터로서, Gamma 분포를 위한 파라미터이다. alpha_0 > 0 이어야 한다.
* beta_0 :  Duration 분포의 하이퍼 파라미터로서, Gamma 분포를 위한 파라미터이다. beta_0 > 0 이어야 한다.

###### Return:
현재 추가된 모델의 인덱스 값을 리턴한다.

- - -

###### pyhsmm.models.MultiParamWeakLimitHDPHSMM.inference(num_sampling)
현재 추가된 모델들에 대해서 Gibbs Sampling을 수행한다. num_sampling 만큼 Iteration을 수행하며, State 업데이트에서 Backward 부분은 tensorflow 기반의 Backward 알고리즘을 사용해서 수행하며, 파라미터 업데이트와 Forward 부분은 CPU 연산을 사용한다.

###### Args:
* num_sampling : Iteration을 몇 번 수행할지를 결정한다. (example. num_sampling = 50 이면 Gibbs Sampling에서 50번의 iteration을 수행한다.)

###### Return:
전체 모델들의 추론된 State 리스트를 [Model Index][StateList] 형태로 리턴한다. 추가된 모델 인덱스 순서대로 리턴하게 된다.

- - -

###### pyhsmm.models.MultiParamWeakLimitHDPHSMM.evaluate(groundtruth)
만약 데이터에 대한 정답 state가 존재한다면, evaluate 함수를 이용해서 accuracy를 측정할 수 있다. 전체 모델들에서 추론된 state 결과와 groundtruth를 전부 비교해서 가장 좋은 성능의 모델을 찾아준다. 이때 추론된 state의 라벨들은 groundtruth에 state 라벨과는 다르게 나오기 때문에, 이에 대한 최적 성능을 찾기위해서 groundtruth의 state의 라벨과 추론된 state의 라벨의 조합에 대해서 가장 큰 정확도를 찾는다.  

###### Args:
* groundtruth : 데이터에 대한 정답 state이다.

###### Return:
best_model_index, accuracys[best_model_index], accuracys 순으로 리턴한다

* best_model_index :  가장 좋은 성능 모델의 index를 리턴한다.
* accuracys[best_model_index] : 가장 좋은 성능 모델의 정확도를 리턴한다.
* accuracys : 전체 모델들의 대한 정확도로서 index 순으로 리턴한다.

- - -

###### pyhsmm.models.MultiParamWeakLimitHDPHSMM.clear_model()
현재 추가되어 있는 모델들을 삭제해주는 함수이다.

- - -

##### pyhsmm.models.MultiParamWeakLimitHDPHSMM.plot(save_dir="./fig/", prefix="model")
전체 모델들의 그래프를 저장해주는 함수이다. save_dir 파라미터에 저장할 위치를 설정 할 수 있다.

###### Args:
* save_dir : 저장하고자 하는 위치를 받는 파라미터이다. 
* prefix: 저장될 그래프 이미지 파일의 prefix를 지정한다. 기본값은 "model" 이며, 기본값의 경우 이미지 파일은 save_dir/model_30_100.png 와 같은 형태로 저장된다.

- - -

## A Simple Example ##
### Generate Sampling data

```python
def generate_data(sample_num, iterate_sampling, obs_num):
    # np.random.seed(1)
    np.seterr(invalid='raise')

    kappa_0_num = 0.01 * (obs_num + 2)
    nu_0_num = obs_num + 2
    num_modes = 3
    groundtruth_states = []

    for i in range(iterate_sampling):
        for j in range(sample_num):
            groundtruth_states.append(i % num_modes)

    obs_hypparams = dict(mu_0=np.zeros(obs_num),
                         sigma_0=np.eye(obs_num),
                         kappa_0=kappa_0_num,
                         nu_0=nu_0_num)

    true_obs_distns = [pyhsmm.distributions.Gaussian(**obs_hypparams) for i in range(num_modes)]
    data = np.concatenate([true_obs_distns[i % num_modes].rvs(sample_num) for i in range(iterate_sampling)])

    return data, obs_hypparams,groundtruth_states
```

샘플링 데이터 생성하는 함수는 위와 같다. 생성할 때는 3가지 파라미터가 필요하다. 먼저 sample_num은 생성되는 데이터에서 하나의 분포의서 몇번의 램덤 샘플링을 할 것인지에 대한 파라미터이다. iterate_sampling은 이러한 샘플링 작업을 몇 번 반복 수행작업을 할 것인지에 대한 파라미터이다. obs_num은 몇 개의 Observation 분포를 사용할지에 대한 파라미터로서 이거의 값에 따라서 Hidden state 개수가 결정된다.

```python
data, obs_hypparams, groundtruth = generate_data(sample_num=3,
                                                 iterate_sampling=40
                                                 obs_num=10)
```

위와 같이 부르면 샘플링 데이터를 생성하는데, 리턴하는 값은 먼저 생성된 data를 리턴해주고, 두 번째로 obs_hypparams는 Observation 분포의 하이퍼 파라미터의 목록을 리턴해주고, 마지막으로 groundtruth로서 정답 hidden state를 리턴해준다.

### 모델 생성 및 파라미터 세팅

```python
# initialize hdphsmm static parameter
hdphsmm = pyhsmm.models.MultiParamWeakLimitHDPHSMM(obs_num=data.shape[1])

# make parameter for hdp
alpha_gamma_list = itertools.product([1e1] * 10,
                                     [1e1] * 10)

# add models
for alpha, gamma in alpha_gamma_list:
    hdphsmm.add_model(alpha, gamma, 6, 2)

# set data for hdphsmm
hdphsmm.set_data(data)
```

### Gibbs Sampling을 이용한 모델들 추론

```python
# hdphsmm start inference with 50th iterations
stateseqs = hdphsmm.inference(50)
```

### 추론된 모델들을 평가

```python
# evaluate between groundtruth and models
best_model_index, best_model_accuracy, accuracys = hdphsmm.evaluate(groundtruth)
print(str(best_model_index)+"th model : " + str(best_model_accuracy))
```

### 추론된 모델들의 그래프를 저장

```python
# save plot
hdphsmm.plot()
```


## References ##
* Matthew J. Johnson. [Bayesian Time Series Models and Scalable
  Inference](http://www.mit.edu/~mattjj/thesis.pdf). MIT PhD Thesis, May 2014.

* Matthew J. Johnson and Alan S. Willsky. [Bayesian Nonparametric Hidden
  Semi-Markov Models](http://www.jmlr.org/papers/volume14/johnson13a/johnson13a.pdf).
  Journal of Machine Learning Research (JMLR), 14:673–701, February 2013.

* Matthew J. Johnson and Alan S. Willsky, [The Hierarchical Dirichlet Process
  Hidden Semi-Markov Model](http://www.mit.edu/~mattjj/papers/uai2010.pdf). 26th
  Conference on Uncertainty in Artificial Intelligence (UAI 2010), Avalon,
  California, July 2010.

* Matt Johnson, Alex Wiltschko, Yarden Katz, Chia-ying (Jackie) Lee,
  Scott Linderman, Kevin Squire, Nick Foti, [PYHSMM Library](https://github.com/mattjj/pyhsmm).