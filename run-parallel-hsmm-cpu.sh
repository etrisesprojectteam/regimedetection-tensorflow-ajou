#!/usr/bin/env bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; pkill -f python; exit 1; }' INT

for i in {1..8}
do
  # echo 'hello'
  python examples/hsmm.py &
done

time wait
